package com.workshop.comp2100.frogger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.Arrays;

import static java.lang.Math.abs;

public class MainActivity extends Activity {

    // gameView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well
    GameView gameView;

    // Helpers for getting swipe direction
    float x1, x2;
    float y1, y2;

    // Game grid
    // key:
    // 0: land (safe)
    // 1: car (unsafe)
    // 2: water (unsafe)
    // 3: log (safe)
    // 9: player
    int[][] gameBoard = {
            {0, 0, 0, 0, 0},
            {2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
    };

    String currentGameScreen = "start";

    // Player pos
    int[] playerPos = {2, 6};

    // car pos
    int[] carPos1 = {2, 4};
    int[] carPos2 = {0, 5};

    // car pos
    int[] logPosA1 = {0, 1};
    int[] logPosA2 = {1, 1};

    // car pos
    int[] logPosB1 = {2, 2};
    int[] logPosB2 = {3, 2};
    int[] logPosB3 = {4, 2};

    // Declare an object of type Bitmap
    Bitmap bitmapBob;

    Bitmap bitmapCar;
    Bitmap bitmapLog;
    Bitmap bitmapWater;
    Bitmap bitmapRoad;


    // Bob starts off not moving
    boolean isMovingUp = false;
    boolean isMovingDown = false;
    boolean isMovingLeft = false;
    boolean isMovingRight = false;

    // He can walk at 150 pixels per second
    float walkSpeedPerSecond = 300;

    int levelSpeed = 6;

    //get move distance based on screen size
    int upDownDistance = getScreenHeight() / 8;
    int sidewaysDistance = getScreenWidth() / 6;


    // start pos of bob the frog
    float bobXPosition = sidewaysDistance * 2;
    float bobYPosition = getScreenHeight() - upDownDistance * 2;

    // start pos of car
    float carXPosition1 = sidewaysDistance * 2;
    float carYPosition1 = upDownDistance * 4;
    float carXPosition2 = sidewaysDistance * 0;
    float carYPosition2 = upDownDistance * 5;

    // start pos of log A
    float LogAXPosition1 = sidewaysDistance * 1;
    float LogAYPosition1 = upDownDistance * 1;
    float LogAXPosition2 = sidewaysDistance * 2;
    float LogAYPosition2 = upDownDistance * 1;

    // start pos of car
    float LogBXPosition1 = sidewaysDistance * 2;
    float LogBYPosition1 = upDownDistance * 2;
    float LogBXPosition2 = sidewaysDistance * 3;
    float LogBYPosition2 = upDownDistance * 2;
    float LogBXPosition3 = sidewaysDistance * 4;
    float LogBYPosition3 = upDownDistance * 2;


    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize gameView and set it as the view
        gameView = new GameView(this);
        setContentView(gameView);

    }

    class GameView extends SurfaceView implements Runnable {

        Thread gameThread = null;

        SurfaceHolder ourHolder;

        // Is the game running
        volatile boolean playing;

        Canvas canvas;
        Paint paint;

        private Paint blackPaint = new Paint();

        // When the we initialize (call new()) on gameView
        // This special constructor method runs
        public GameView(Context context) {
            // The next line of code asks the
            // SurfaceView class to set up our object.
            // How kind.
            super(context);

            // Initialize ourHolder and paint objects
            ourHolder = getHolder();
            paint = new Paint();

            // Load Bob from his .png file
            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);

            // scale down image
            bitmapBob = (Bitmap.createScaledBitmap(bitmapBob, 300, 300, false));

            // Load Bob from his .png file
            bitmapCar = BitmapFactory.decodeResource(this.getResources(), R.drawable.car);

            // scale down image
            bitmapCar = (Bitmap.createScaledBitmap(bitmapCar, 300, 200, false));

            // Load Bob from his .png file
            bitmapLog = BitmapFactory.decodeResource(this.getResources(), R.drawable.log);

            // scale down image
            bitmapLog = (Bitmap.createScaledBitmap(bitmapLog, 300, 200, false));

            // Load Bob from his .png file
            bitmapWater = BitmapFactory.decodeResource(this.getResources(), R.drawable.water);

            // scale down image
            bitmapWater = (Bitmap.createScaledBitmap(bitmapWater, 300, 300, false));

            // Load Bob from his .png file
            bitmapRoad = BitmapFactory.decodeResource(this.getResources(), R.drawable.road);

            // scale down image
            bitmapRoad = (Bitmap.createScaledBitmap(bitmapRoad, 300, 300, false));



            // Set our boolean to true - game on!
            playing = true;

        }

        @Override
        public void run() {

            try {
                gameLoop();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

        public void ended() {
            if (playerPos[1] == 0) {

                Intent myIntent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(myIntent);

            }
        }

        public void calculate() {

            if (gameBoard[playerPos[1]][playerPos[0]] == 0 || gameBoard[playerPos[1]][playerPos[0]] == 3) {
                // safe spaces
                playing = true;
            } else {
                playing = false;
                // dead
            }

            ended();

        }

        public void gameLoop () throws InterruptedException {
            while (playing) {
                // Update the frame
                update();

                //Calculate gamestate
                calculate();

                // Draw the frame
                draw();

                Thread.sleep(500); //the timing mechanism
            }
        }

        public void update() {


            // If bob is moving (the player is touching the screen)
            // then move him to the right based on his target speed and the current fps.
            if (isMovingUp && playerPos[1] > 0){
                // updates the frog pos
                bobYPosition = bobYPosition - upDownDistance;
                //updates the state pos
                playerPos[1] = playerPos[1] - 1;
                isMovingUp = false;
            }

            if (isMovingDown && playerPos[1] < 6){
                bobYPosition = bobYPosition + upDownDistance;
                playerPos[1] = playerPos[1] + 1;
                isMovingDown = false;
            }

            if (isMovingLeft && playerPos[0] > 0){
                bobXPosition = bobXPosition - sidewaysDistance;
                playerPos[0] = playerPos[0] - 1;
                isMovingLeft = false;
            }

            if (isMovingRight && playerPos[0] < 4){
                bobXPosition = bobXPosition + sidewaysDistance;
                playerPos[0] = playerPos[0] + 1;
                isMovingRight = false;
            }


            if (carPos2[0] < 4) {
                carXPosition2 = carXPosition2 + sidewaysDistance;
                gameBoard[carPos2[1]][carPos2[0]] = 0;
                carPos2[0] = carPos2[0] + 1;
                gameBoard[carPos2[1]][carPos2[0]] = 1;
            } else {
                carXPosition2 = 0;
                gameBoard[carPos2[1]][carPos2[0]] = 0;
                carPos2[0] = 0;
                gameBoard[carPos2[1]][carPos2[0]] = 1;
            }

            if (carPos1[0] < 4) {
                carXPosition1 = carXPosition1 + sidewaysDistance;
                gameBoard[carPos1[1]][carPos1[0]] = 0;
                carPos1[0] = carPos1[0] + 1;
                gameBoard[carPos1[1]][carPos1[0]] = 1;
            } else {
                carXPosition1 = 0;
                gameBoard[carPos1[1]][carPos1[0]] = 0;
                carPos1[0] = 0;
                gameBoard[carPos1[1]][carPos1[0]] = 1;
            }

//
//                ///////////////////////////////////////////////////////

            if (logPosA2[0] < 4) {
                LogAXPosition2 = LogAXPosition2 + sidewaysDistance;
                gameBoard[logPosA2[1]][logPosA2[0]] = 2;
                logPosA2[0] = logPosA2[0] + 1;
                gameBoard[logPosA1[1]][logPosA1[0]] = 3;
            } else {
                LogAXPosition2 = 0;
                gameBoard[logPosA2[1]][logPosA2[0]] = 2;
                logPosA2[0] = 0;
                gameBoard[logPosA2[1]][logPosA2[0]] = 3;
            }

            if (logPosA1[0] < 4) {
                LogAXPosition1 = LogAXPosition1 + sidewaysDistance;
                gameBoard[logPosA1[1]][logPosA1[0]] = 2;
                logPosA1[0] = logPosA1[0] + 1;
                gameBoard[logPosA1[1]][logPosA1[0]] = 3;
            } else {
                LogAXPosition1 = 0;
                gameBoard[logPosA1[1]][logPosA1[0]] = 2;
                logPosA1[0] = 0;
                gameBoard[logPosA1[1]][logPosA1[0]] = 3;
            }
//              __________________________________

            if (logPosB1[0] > 0) {
                LogBXPosition1 = LogBXPosition1 - sidewaysDistance;
                gameBoard[logPosB1[1]][logPosB1[0]] = 2;
                logPosB1[0] = logPosB1[0] - 1;
                gameBoard[logPosB1[1]][logPosB1[0]] = 3;
            } else {
                LogBXPosition1 = sidewaysDistance * 4;
                gameBoard[logPosB1[1]][logPosB1[0]] = 2;
                logPosB1[0] = 4;
                gameBoard[logPosB1[1]][logPosB1[0]] = 3;
            }

            if (logPosB2[0] > 0) {
                LogBXPosition2 = LogBXPosition2 - sidewaysDistance;
                gameBoard[logPosB2[1]][logPosB2[0]] = 2;
                logPosB2[0] = logPosB2[0] - 1;
                gameBoard[logPosB2[1]][logPosB2[0]] = 3;
            } else {
                LogBXPosition2 = sidewaysDistance * 4;
                gameBoard[logPosB2[1]][logPosB2[0]] = 2;
                logPosB2[0] = 4;
                gameBoard[logPosB2[1]][logPosB2[0]] = 3;
            }

            if (logPosB3[0] > 0) {
                LogBXPosition3 = LogBXPosition3 - sidewaysDistance;
                gameBoard[logPosB3[1]][logPosB3[0]] = 2;
                logPosB3[0] = logPosB3[0] - 1;
                gameBoard[logPosB3[1]][logPosB3[0]] = 3;
            } else {
                LogBXPosition3 = sidewaysDistance * 4;
                gameBoard[logPosB3[1]][logPosB3[0]] = 2;
                logPosB3[0] = 4;
                gameBoard[logPosB3[1]][logPosB3[0]] = 3;
            }
//

            System.out.println(Arrays.deepToString(gameBoard));
            System.out.println((Arrays.toString(playerPos)));


        }

        public void draw() {

            if (currentGameScreen == "start") {
                // show start screen
            } else if (currentGameScreen == "play") {
                // Show play game screen
            } else if (currentGameScreen == "Game Over") {
                //show end screen
            } else {
                currentGameScreen = "start";
            }

            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {
                // Lock the canvas ready to draw
                canvas = ourHolder.lockCanvas();

                // Draw the background color
                canvas.drawColor(Color.argb(255,  40, 208, 182));

                // Choose the brush color for drawing
                paint.setColor(Color.argb(255,  109, 249, 0));

                // Make the text a bit bigger
                paint.setTextSize(45);

                paint.setStyle(Paint.Style.STROKE);

                for (int i = 0; i < 7; i++) {
                    for (int j = 0; j < 7; j++) {
                        canvas.drawLine(0, j * upDownDistance,
                                1200, j * upDownDistance,
                                blackPaint);

                        if (j == 2 || j == 1) {
                            canvas.drawBitmap(bitmapWater, i * sidewaysDistance, j * upDownDistance, paint);
                        }

                        if (j == 4 || j == 5) {
                            canvas.drawBitmap(bitmapRoad, i * sidewaysDistance, j * upDownDistance, paint);
                        }


                    }
                }
//
                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapCar, carXPosition1, carYPosition1, paint);

                // Draw bob at bobXPosition, bobYPosition
                // + 100 helps pus image off the map a bit so it looks like its leaving
                canvas.drawBitmap(bitmapCar, carXPosition2, carYPosition2, paint);

                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapLog, LogAXPosition1, LogAYPosition1, paint);

                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapLog, LogAXPosition2, LogAYPosition2, paint);

                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapLog, LogBXPosition1, LogBYPosition1, paint);

                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapLog, LogBXPosition2, LogBYPosition2, paint);

                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapLog, LogBXPosition3, LogBYPosition3, paint);



                // Draw bob at bobXPosition, bobYPosition
                canvas.drawBitmap(bitmapBob, bobXPosition, bobYPosition - 100, paint);

                // Draw everything to the screen
                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN:
                {
                    x1 = motionEvent.getX();
                    y1 = motionEvent.getY();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    x2 = motionEvent.getX();
                    y2 = motionEvent.getY();

                    //if left to right sweep event on screen
                    if (x1 < x2) {
                        if (abs(x1 - x2) > abs(y1 - y2)) {
                            isMovingRight = true;
                        }
                    }

                    // if right to left sweep event on screen
                    if (x1 > x2) {
                        if (abs(x1 - x2) > abs(y1 - y2)) {
                            isMovingLeft = true;
                        }
                    }

                    // if UP to Down sweep event on screen
                    if (y1 < y2) {
                        if (abs(y1 - y2) > abs(x1 - x2)) {
                            isMovingDown = true;
                        }
                    }

                    //if Down to UP sweep event on screen
                    if (y1 > y2) {
                        if (abs(y1 - y2) > abs(x1 - x2)) {
                            isMovingUp = true;
                        }
                    }
                    break;
                }
            }
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        gameView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Tell the gameView pause method to execute
        gameView.pause();
    }

}
